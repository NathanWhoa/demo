import AVKit

class ViewController: AVPlayerViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if let url = videoURL(forName: videoName) {
            player = AVPlayer(url: url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        player?.play()
    }
    
    private func setupUI() {
        showsPlaybackControls = false
        videoGravity = .resizeAspectFill
    }
    
    private func videoURL(forName name: String) -> URL? {
        for fileExtention in fileExtensions {
            if let soundURL = Bundle.main.url(forResource: name, withExtension: fileExtention) {
                return soundURL
            }
        }
        fatalError("Unable to find sound file with name '\(name)'")
    }
}

