/// Possible file extensions for videos
let fileExtensions = [
    "mp4",
    "mov"
]

/// The name of video without extension
let videoName = "video"
